// This file includes simplified functions

// Extend localStorage & sessionStorage to handle arrays/objects
// @see http://stackoverflow.com/questions/3357553#10109307
Storage.prototype.setObj = function (key, obj) {
    return this.setItem(key, JSON.stringify(obj))
}

// Extend localStorage & sessionStorage to handle arrays/objects
// @see http://stackoverflow.com/questions/3357553#10109307
Storage.prototype.getObj = function (key) {
    return JSON.parse(this.getItem(key))
}

// Extend Array
// @see http://stackoverflow.com/questions/1988349#1988361
// Check if an element exists in array using a comparer function
// comparer : function(currentElement)
Array.prototype.inArray = function (comparer) {
    for (var i = 0; i < this.length; i++) {
        if (comparer(this[i])) return true;
    }
    return false;
};

// Extend Array
// @see http://stackoverflow.com/questions/1988349#1988361
// Adds an element to the array if it does not already exist using a comparer
// function
Array.prototype.pushIfNotExist = function (element, comparer) {
    if (!this.inArray(comparer)) {
        this.push(element);
    }
};

// Converts any string into color
// @see http://stackoverflow.com/questions/3426404#16348977
var stringToColor = function(str) {

    // str to hash
    for (var i = 0, hash = 0; i < str.length; hash = str.charCodeAt(i++) + ((hash << 5) - hash));

    // int/hash to hex
    for (var i = 0, hex = ""; i < 3; hex += ("00" + ((hash >> i++ * 8) & 0xFF).toString(16)).slice(-2));

    // hex to RGB
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return [r, g, b].join();
}