$(document).ready(function () {
    //$('a').click(function (event) {
    //    event.preventDefault();
    //    $(this).hide("slow");
    //});

    // Initialize app on load
    app.init();

    renderTable();
});

// Shipping App
app = {
    // Initialize, i.e. create dummy client-side db
    init: function () {
        // Goods
        var goods = this.goods.list();
        if (!goods) this.goods.save({});

        // Packages
        var packages = this.packages.list();
        if (!packages) this.packages.save({});

        // limitedShipping
        var status = this.status.list();
        if (!status) this.status.save({});
    },

    remove: function () {
        app.status.remove();
        app.packages.remove();
        app.goods.remove();
    },

    goods: {
        // Add Good
        add: function (name, isHazmat, limit) {
            this.edit(name, isHazmat, limit);
        },

        // Edit Good
        edit: function (name, isHazmat, limit, uuid) {
            // Validate / Set Defaults
            // - Optionally, check against duplicate UUID
            uuid = typeof uuid !== 'undefined' ? uuid : generateUUID();
            // - name
            if (typeof name === 'undefined') {
                console.error('No name specified');
                return;
            }
            isHazmat = typeof isHazmat !== 'undefined' ? isHazmat : false;
            limit = typeof limit !== 'undefined' ? limit : 0;

            // Process
            // - Load Existing
            var goods = this.list();
            // - Prepare
            // -- If current package doesn't exist, make object.
            goods[uuid] = typeof goods[uuid] !== 'undefined' ? goods[uuid] : {};
            // -- Update uuid & name
            goods[uuid]['uuid'] = uuid;
            goods[uuid]['name'] = name;
            goods[uuid]['isHazmat'] = isHazmat;
            goods[uuid]['limit'] = limit;
            // - Save
            this.save(goods);
            // - Refresh Status
            app.status.refresh();
        },

        // Remove Goods
        remove: function (uuid) {
            // Process
            // - Remove All Goods
            if (typeof uuid === 'undefined') {
                goods = {};
            }
            // - Remove a Specific Good
            else {
                // Load Existing
                var goods = this.list();
                // Prepare
                delete goods[uuid];
            }
            // - Save Packages
            this.save(goods);
            // - Refresh Status
            app.status.refresh();
        },

        // List Goods
        list: function (head) {
            // Process
            var output = localStorage.getObj('goods');
            if (head === true) {
                var objectHead = {
                    'uuid': 'Uuid',
                    'name': 'Name',
                    'isHazmat': 'Is Hazmat',
                    'limit': 'Limit',
                }
                output['head'] = objectHead;
            }

            return output;
        },

        // Save Goods
        save: function (goods) {
            // Validate / Set Defaults
            // - goods
            if (typeof goods === 'undefined') {
                console.error('No goods specified');
                return;
            }

            localStorage.setObj('goods', goods);

            // Refresh Table
            renderTable();
        },

        // Render Goods
        render: function () {
            var goods = this.list();

            var output = '<table>';

            output += '<tr><th>Name</th><th>Is Hazmat</th><th>Limit</th><th></th></tr>';

            for (var uuid in goods) {
                var name = goods[uuid]['name'];
                var limit = goods[uuid]['limit'];
                var isHazmat = goods[uuid]['isHazmat'];
                output += '<tr id="' + uuid + '">';
                output += '<td class="name">' + name + '</td>';
                output += '<td class="isHazmat ' + isHazmat + '">' + isHazmat + '</td>';
                output += '<td class="limit">' + limit + '</td>';
                output += '<td class="actions"><a class="remove" onclick="event.preventDefault(); app.goods.remove(\'' + uuid + '\');" href="#"><span class="text">Remove</span></a><span class="connect"><input type="number" name="count"><a class="link" onclick="event.preventDefault(); form.packages.linkGoods(this);" href="#"><span class="text">Link</span></a></span></td>';
                output += '</tr>';
            }

            output += '</table>';

            return output;
        }
    },

    packages: {
        // Add Package
        add: function (name) {
            this.edit(name);
        },

        // Edit Good
        edit: function (name, uuid) {
            // Validate / Set Defaults
            // - name
            if (typeof name === 'undefined') {
                console.error('No name specified');
                return;
            }
            // - Optionally, check against duplicate UUID
            uuid = typeof uuid !== 'undefined' ? uuid : generateUUID();

            // Process
            // - Load Existing
            var packages = this.list();
            // - Prepare
            // -- If current package doesn't exist, make new object.
            packages[uuid] = typeof packages[uuid] !== 'undefined' ? packages[uuid] : {};
            // -- Update uuid & name
            packages[uuid]['uuid'] = uuid;
            packages[uuid]['name'] = name;
            // - Save
            this.save(packages);
            // - Refresh Status
            app.status.refresh(uuid);
        },

        // Remove Package
        remove: function (uuid) {
            // Process
            // - Remove All Packages
            if (typeof uuid === 'undefined') {
                package = {};
            }
            // - Remove a Specific Package
            else {
                // Load Existing
                var package = this.list();
                // Prepare
                delete package[uuid];
            }
            // - Save Packages
            this.save(package);

            // Refresh Status
            app.status.refresh(uuid);
        },

        // List Packages
        list: function () {
            return localStorage.getObj('packages')
        },

        // Save Packages
        save: function (packages) {
            // Validate / Set Defaults
            // - packages
            if (typeof packages === 'undefined') {
                console.error('No packages specified');
                return;
            }

            localStorage.setObj('packages', packages);

            // Refresh Table
            renderTable();
        },

        // Render Packages
        render: function () {
            var packages = this.list();
            var goods = app.goods.list();
            var status = app.status.list();

            var output = '<table>';
            output += '<tr class="d1"><th>Name</th><th>Has Hazmat Goods</th><th>Limited Quantity</th><th>Goods</th><th></th></tr>';
            for (var uuid in packages) {
                var name = packages[uuid]['name'];
                var isHazmat = typeof status[uuid] !== 'undefined' ? status[uuid]['isHazmat'] : '<span class="error">Missing!</span>';
                var limitedShipping = typeof status[uuid] !== 'undefined' ? status[uuid]['limitedShipping'] : '<span class="error">Missing!</span>';
                output += '<tr id="' + uuid + '" class="d1">';
                output += '<td class="name">' + name + '</td>';
                output += '<td class="isHazmat">' + isHazmat + '</td>';
                output += '<td class="limitedShipping">' + limitedShipping + '</td>';
                var packageGoods = packages[uuid]['goods'];
                output += '<td>';
                output += '<table>';
                output += '<tr><th>Name</th><th>Qnt/Limit</th><th></th></tr>';
                for (var goodUuid in packageGoods) {
                    var name = typeof goods[goodUuid] !== 'undefined' ? goods[goodUuid]['name'] : '<span class="error">Missing!</span>';
                    var count = packageGoods[goodUuid]['count'];
                    var limit = typeof goods[goodUuid] !== 'undefined' ? goods[goodUuid]['limit'] : '<span class="error">Missing!</span>';
                    limit = limit == 0 ? '~' : limit;
                    output += '<tr>';
                    output += '<td class="name">' + name + '</td>';
                    output += '<td class="count">' + count + '/' + limit + '</td>';
                    output += '<td class="actions"><a class="remove" onclick="event.preventDefault(); app.packages.removeGoods(\'' + uuid + '\', \'' + goodUuid + '\');" href="#"><span class="text">Remove</span></a></td>';
                    output += '</tr>';
                }
                output += '<tr><td></td><td class="actions"><a onclick="event.preventDefault(); form.packages.addGoods(this);" class="add" href="#"><span class="text">Add</span></a></td><td></td></tr>';
                output += '</table>';
                output += '</td>';
                output += '<td class="actions"><a class="remove" onclick="event.preventDefault(); app.packages.remove(\'' + uuid + '\');" href="#"><span class="text">Remove</span></a></td>';
                output += '</tr>';
            }
            output += '</table>';

            return output;
        },

        // Add Package Goods
        addGoods: function (packageUuid, goodUuid, count) {
            this.editGoods(packageUuid, goodUuid, count);
        },

        // Edit Package Goods
        editGoods: function (packageUuid, goodUuid, count) {
            // Validate / Set Defaults
            // - packageUuid
            if (typeof packageUuid === 'undefined') {
                console.error('No package uuid specified');
                return;
            }
            // - goodUuid
            if (typeof goodUuid === 'undefined') {
                console.error('No goods uuid referenced');
                return;
            }
            // - count
            if (typeof count === 'undefined') {
                console.error('No goods count specified');
                return;
            }

            // Process
            // - Load Existing
            var packages = this.list();
            var goods = app.goods.list();
            // - Validate
            // -- If uuid of package doesn't exist, exit
            if (typeof packages[packageUuid] === 'undefined') {
                console.error('Package Uuid is not found');
                return;
            }
            // -- If goodsUuid doesn't exist, exit
            if (typeof goods[goodUuid] === 'undefined') {
                console.error('Goods Uuid is not found');
                return;
            }
            // - Prepare
            // -- If goods object doesn't exist, make new object.
            packages[packageUuid]['goods'] = typeof packages[packageUuid]['goods'] !== 'undefined' ? packages[packageUuid]['goods'] : {};
            var good = {'uuid': goodUuid, 'count': count};
            // -- Update goodUuid
            packages[packageUuid]['goods'][goodUuid] = good;
            // - Save Packages
            localStorage.setObj('packages', packages);
            // - Refresh Status
            app.status.refresh(packageUuid);
        },
        // Remove Package Good
        removeGoods: function (packageUuid, goodUuid) {
            // Validate / Set Defaults
            // - packageUuid
            if (typeof packageUuid === 'undefined') {
                console.error('No packageUuid specified');
                return;
            }

            // Process
            // - Load Existing
            var packages = this.list();
            // - Remove All Goods from Package
            if (typeof goodUuid === 'undefined') {
                packages[packageUuid]['goods'] = {}
            }
            // - Remove a Specific Good from Package
            else {
                // Prepare
                delete packages[packageUuid]['goods'][goodUuid];
            }
            // - Save Packages
            localStorage.setObj('packages', packages);
            // - Refresh Status
            app.status.refresh(packageUuid);

            // Refresh Table
            renderTable();
        },
    },

    status: {
        // Reset Packages in the Status
        refresh: function (uuid) {

            // Process All
            if (typeof uuid === 'undefined') {
                var packages = app.packages.list();
                // - Load Existing
                var packages = app.packages.list();
                // Loop through every Uuid to refresh everything
                for (var packageGoods in packages) {
                    this.refresh(packageGoods);
                }
                return;
            }

            // Process Uuid
            // - Load Existing
            var packages = app.packages.list();
            // - Process / Save
            if (typeof packages[uuid] === 'undefined') {
                delete this.remove(uuid);
                return;
            }

            // Process Added/Updated
            // - Load Existing
            var goods = app.goods.list();
            var status = this.list();
            // - Prepare
            var packageGoods = packages[uuid]['goods'];
            // -- limitedShipping - For each package[goods], check count against limit
            var setLimitedShipping = true;
            for (var goodUuid in packageGoods) {
                // If no DB for Package Good, set Missing!
                if (typeof goods[goodUuid] === 'undefined') {
                    setLimitedShipping = '<span class="error">Missing!</span>';
                    break;
                }
                var count = packageGoods[goodUuid]['count'];
                var limit = goods[goodUuid]['limit'];
                // If goods are limited (!0) && there are more goods than limit, package is not qualified for limited shipping
                if (+limit > 0 && (+count > +limit)) {
                    setLimitedShipping = false;
                }
            }
            // -- isHazmat
            var setIsHazmat = false;
            for (var goodUuid in packageGoods) {
                // If no DB for Package Good, set Missing!
                if (typeof goods[goodUuid] === 'undefined') {
                    setIsHazmat = '<span class="error">Missing!</span>';
                    break;
                }
                var isHazmat = goods[goodUuid]['isHazmat'];
                // If good is hazmat, set package to hazmat
                if (isHazmat) {
                    setIsHazmat = true;
                }
            }
            // If goods object doesn't exist, make new object.
            status[uuid] = typeof status[uuid] !== 'undefined' ? status[uuid] : {};
            status[uuid]['uuid'] = uuid;
            status[uuid]['limitedShipping'] = setLimitedShipping;
            status[uuid]['isHazmat'] = setIsHazmat;
            // - Save
            this.save(status);
        },

        // Remove Status
        remove: function (uuid) {
            // Process
            // - Remove All Statuses
            if (typeof uuid === 'undefined') {
                status = {};
            }
            // - Remove a Specific Package
            else {
                // Load Existing
                var status = this.list();
                // Prepare
                delete status[uuid];
            }
            // - Save Status
            this.save(status);
        },

        // List Packages from the Status
        list: function () {
            return localStorage.getObj('status');
        },

        // Save Packages in the Status
        save: function (status) {
            // Validate / Set Defaults
            // - packages
            if (typeof status === 'undefined') {
                console.error('No status specified');
                return;
            }

            localStorage.setObj('status', status);

            // Refresh Table
            renderTable();
        }
    }
}

// Gui - add goods/packages
form = {
    goods: {
        create: function () {
            $('#goods table tr:last').after('<tr><td><input type="text" name="name"></td><td><input type="checkbox" name="isHazmat"></td><td><input type="number" name="limit"></td><td class="actions"><a class="save" onclick="event.preventDefault(); form.goods.save();" href="#"><span class="text">Save</span></a></td></tr>');
        },
        save: function () {
            var name = $('#goods table tr:last input[name="name"]').val();
            var isHazmat = $('#goods table tr:last input[name="isHazmat"]').is(':checked');
            var limit = $('#goods table tr:last input[name="limit"]').val();

            app.goods.add(name, isHazmat, limit)
        }
    },

    packages: {
        create: function () {
            $('#packages table tr.d1:last').after('<tr class="d1"><td><input type="text" name="name"></td><td></td><td></td><td></td><td class="actions"><a class="save" onclick="event.preventDefault(); form.packages.save();" href="#"><span class="text">Save</span></a></td></tr>');
        },
        save: function () {
            var name = $('#packages table tr.d1:last input[name="name"]').val();

            app.packages.add(name)
        },
        addGoods: function (elmnt) {
            $(elmnt).closest('tr.d1').addClass('active');
            $('#goods .connect').show();
        },
        linkGoods: function (elmnt) {
            var packageUuid = $("#packages tr.d1.active").attr('id');
            var row = $(elmnt).closest("tr");
            var goodUuid = row.attr('id');
            var count = row.find("input[name='count']").val();
            app.packages.addGoods(packageUuid, goodUuid, count);
        }
    }
}

function addGoods() {
}

function saveGoods() {
    ;
}

// Populates db with dummy content
function insertDummyContent() {
    // Process
    // - Prepare
    var goods = {
        '6ec53c94-c34b-456b-bb98-227b55be7d94': {
            'uuid': '6ec53c94-c34b-456b-bb98-227b55be7d94',
            'name': 'apples',
            'isHazmat': true,
            'limit': 13
        },
        '8da85c4d-7c9a-4d1a-bc53-1696dd1209cc': {
            'uuid': '8da85c4d-7c9a-4d1a-bc53-1696dd1209cc',
            'name': 'pickles',
            'isHazmat': false,
            'limit': 0
        },
        '6eec2a9d-329e-4152-9461-f7608ec96711': {
            'uuid': '6eec2a9d-329e-4152-9461-f7608ec96711',
            'name': 'bananas',
            'isHazmat': true,
            'limit': 5
        },
        '26160a39-27d0-48ea-aee0-6f76e58512fa': {
            'uuid': '26160a39-27d0-48ea-aee0-6f76e58512fa',
            'name': 'oranges',
            'isHazmat': true,
            'limit': 8
        }
    }

    var packages = {
        '7836816b-da24-431d-959a-1ec23ad141ee': {
            'name': 'washington',
            'uuid': '7836816b-da24-431d-959a-1ec23ad141ee',
            'goods': {
                '8da85c4d-7c9a-4d1a-bc53-1696dd1209cc': {
                    'uuid': '8da85c4d-7c9a-4d1a-bc53-1696dd1209cc',
                    'count': '10'
                }
            }
        },
        '6ec53c94-c34b-456b-bb98-227b55be7d94': {
            'name': 'oregon',
            'uuid': '6ec53c94-c34b-456b-bb98-227b55be7d94',
            'goods': {
                '26160a39-27d0-48ea-aee0-6f76e58512fa': {
                    'uuid': '26160a39-27d0-48ea-aee0-6f76e58512fa',
                    'count': '10'
                },
                '6eec2a9d-329e-4152-9461-f7608ec96711': {
                    'uuid': '6eec2a9d-329e-4152-9461-f7608ec96711',
                    'count': '7'
                },
                '6ec53c94-c34b-456b-bb98-227b55be7d94': {
                    'uuid': '6ec53c94-c34b-456b-bb98-227b55be7d94',
                    'count': '7'
                }
            }
        },
        'db5a7bcb-d81b-4bdb-a042-21e1803d834a': {
            'name': 'california',
            'uuid': 'db5a7bcb-d81b-4bdb-a042-21e1803d834a',
            'goods': {
                '6eec2a9d-329e-4152-9461-f7608ec96711': {
                    'uuid': '6eec2a9d-329e-4152-9461-f7608ec96711',
                    'count': '6'
                },
                '6ec53c94-c34b-456b-bb98-227b55be7d94': {
                    'uuid': '6ec53c94-c34b-456b-bb98-227b55be7d94',
                    'count': '1'
                }
            }
        }
    }
    // - Save Goods
    app.goods.save(goods);
    // - Save Packages
    app.packages.save(packages);
    // - Refresh Status
    app.status.refresh();
};

// Render
function renderTable() {
    $('#goods').html(app.goods.render());
    $('#packages').html(app.packages.render());
}

// Generate Universally Unique ID / UUID
// @see http://stackoverflow.com/questions/105034#8809472
function generateUUID() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now();
        //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}